#ifndef MOUNTER_H
#define MOUNTER_H

int HttpDiskMountUrl(int DeviceNumber, const char * Url, char DriveLetter, BOOLEAN CdImage);
int HttpDiskUmount(char DriveLetter);

#endif