#ifndef LIST_H
#define LIST_H

#define list_add(Type, Head, Item) do {\
			Item->next = Head; \
			Head = Item; \
		} while (0);

#define list_remove(Type, Head, Item) do {\
			Type **p; \
			for (p = &Head; *p; p = &((*p)->next)) { \
				if (*p = Item) { \
					*p = Item->next; \
				} \
			} \
		} while (0);

#define list_foreach(Type, Head, Item) \
	for (Item = Head; Item; Item = Item->next)

#endif